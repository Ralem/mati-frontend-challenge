module.exports = {
    parser: 'babel-eslint',
    parserOptions: {
        root: true,
    },
    extends: [
        'standard'
    ],
    plugins: [
        'html',
        'vue',
    ],
    'rules': {
        'comma-dangle': ['off'],
        'padded-blocks': 0,
        'no-unused-vars': ['warn'],
        'no-undef': ['warn'],
        'quotes': ['off'],
        'no-console': ['off'],
        'max-len': ['off'],
        'semi': ['off', 'always'],
        'space-before-blocks': ['off'],
        'space-before-function-paren': ['off'],
        'camelcase': ['warn'],
        'comma-style': ['warn', 'last'],
        'spaced-comment': ['off'],
        'indent': [1, 4, {
            'SwitchCase': 1
        }],
    }
}
