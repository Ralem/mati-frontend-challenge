import Vue from "vue";
import App from "./App.vue";
import "@babel/polyfill";
import "es6-promise/auto";
import "sanitize.css";
import "styles/app.styl";

const render = function() {
    const app = new Vue({
        el: "#app",
        render: h => h(App),
    });
};
render();
