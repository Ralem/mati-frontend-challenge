'use strict'
const path = require("path");
const { VueLoaderPlugin } = require("vue-loader");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const dev = {
    mode: "development",
    entry: [
        "./src/app.js",
    ],
    devServer: {
        hot: true,
        watchOptions: {
            poll: true,
        }
    },
    resolve: {
        extensions: [".js", ".vue"],
        alias: {
            styles: path.resolve("src/styles"),
            components: path.resolve("src/components"),
        }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: "vue-loader",
            },
            {
                test: /\.styl(us)?$/,
                use: [
                    "vue-style-loader",
                    "css-loader",
                    "stylus-loader",
                ],
            },
            {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: "index.html",
            inject: true
        }),
    ]
};

module.exports = dev;
